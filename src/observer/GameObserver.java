package observer;

import enums.FieldType;
import game.Turn;

/**
 * Das Spiel informiert die GameObserver mit den hier aufgührten Methoden.
 */
public interface GameObserver {

    /**
     * Informiert den GameObserver über einen neuen Spielzug.
     * 
     * @param t
     *            der Zug
     */
    void pieceWasSet(Turn t);

    /**
     * Informiert den GameObserver, dass das spiel vorbei ist
     *
     * @param type
     *          beinhalten den Type des gewinners, FieldType.EMPTY wenn es ein unentschieden ist
     */
    void gameHasEnded(FieldType type);

}
