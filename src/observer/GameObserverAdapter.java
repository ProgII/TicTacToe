package observer;

import enums.FieldType;
import game.Turn;

public abstract class GameObserverAdapter implements GameObserver {

	@Override
	public void pieceWasSet(Turn t) {
	}

    @Override
    public void gameHasEnded(FieldType type) {
    }

}
