package observer;

public interface GameObservable {

    /**
     * Fügt einen GameObserver hinzu.
     *
     * @param gameObserver
     *            the GameObserver to add
     */
    void addObserver(GameObserver gameObserver);

    /**
     * Löscht einen GameObserver.
     *
     * @param gameObserver
     *            the GameObserver to remove
     */
    void removeObserver(GameObserver gameObserver);

}
