package player;

import java.util.Random;

import board.Board;
import board.BoardInfo;
import enums.FieldType;
import game.GameInfo;
import game.Turn;
import observer.GameObserver;
import observer.GameObserverAdapter;

public class AutoPlayer extends GameObserverAdapter implements  Player {

    private final Random r = new Random();
    private final BoardInfo board;
    private final GameInfo game;
    private FieldType myType;
    private FieldType enemyType;

    public AutoPlayer(GameInfo gameInfo) {
    	this.game = gameInfo;
        this.board = game.getBoardInfo();
    }

    private class Pair {
        int x, y;

        Pair(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }

    private Pair createMissingFieldForWin(FieldType type, int x1, int y1, int x2, int y2, int x3, int y3) {
        FieldType k1 = this.board.getFieldInfo(x1, y1);
        FieldType k2 = this.board.getFieldInfo(x2, y2);
        FieldType k3 = this.board.getFieldInfo(x3, y3);

        if (k1 == FieldType.empty && k2 == type && k3 == type)
            return new Pair(x1, y1);

        if (k1 == type && k2 == FieldType.empty && k3 == type)
            return new Pair(x2, y2);

        if (k1 == type && k2 == type && k3 == FieldType.empty)
            return new Pair(x3, y3);

        return null;
    }


    private Pair[] createFinishMoves(FieldType type) {
        Pair[] curr = new Pair[8];
        //Diagonale
        curr[0] = createMissingFieldForWin(type, 0, 0, 1, 1, 2, 2);
        curr[7] = createMissingFieldForWin(type, 0, 2, 1, 1, 2, 0);

        //Senkrechte
        curr[1] = createMissingFieldForWin(type, 0, 0, 0, 1, 0, 2);
        curr[2] = createMissingFieldForWin(type, 1, 0, 1, 1, 1, 2);
        curr[3] = createMissingFieldForWin(type, 2, 0, 2, 1, 2, 2);

        //Waagerechte
        curr[4] = createMissingFieldForWin(type, 0, 0, 1, 0, 2, 0);
        curr[5] = createMissingFieldForWin(type, 0, 1, 1, 1, 2, 1);
        curr[6] = createMissingFieldForWin(type, 0, 2, 1, 2, 2, 2);
        return curr;
    }

    private Turn generateTurn() {

        Pair[] curr = createFinishMoves(myType);

        for (Pair p : curr) {
            if (p != null) {
                System.out.println(p.x + " " + p.y);
                return new Turn(p.x, p.y, this.myType);
            }
        }

        Pair[] curr1 = createFinishMoves(enemyType);
        for (Pair p : curr1) {
            if (p != null) {
                System.out.println(p);
                return new Turn(p.x, p.y, this.myType);
            }
        }
        int x, y;
        do {
            x = r.nextInt(3);
            y = r.nextInt(3);
        } while (!this.board.canSetPiece(x, y));


        return new Turn(x, y, this.myType);
    }

    @Override
    public Turn getNextTurn() {
        return generateTurn();
    }

    @Override
    public void setFieldType(FieldType k) {
        if (k == FieldType.O) enemyType = FieldType.X;
        else enemyType = FieldType.O;
        this.myType = k;
    }

    @Override
    public FieldType getFieldType() {
        return this.myType;
    }
    
}
