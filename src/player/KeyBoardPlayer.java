package player;

import java.util.Scanner;

import enums.FieldType;
import game.Turn;
import observer.GameObserverAdapter;

/**
 * Diese Klasse implementiert einen Spieler, der über die Kommandozeile
 * gesteuert werden kann.
 */
public class KeyBoardPlayer extends GameObserverAdapter implements  Player {

    private FieldType fieldType;
    private Scanner scanner = new Scanner(System.in);

    public KeyBoardPlayer() {

    }

    @Override
    public Turn getNextTurn() {
        System.out.println("Sie sind am Zug!");
        System.out.println("x Koordinate bitte (0-2)");
        int x = scanner.nextInt();
        System.out.println("y Koordinate bitte (0-2)");
        int y = scanner.nextInt();
        return new Turn(x, y, this.fieldType);
    }

    @Override
    public void setFieldType(FieldType k) {
        this.fieldType = k;
        System.out.println("Sie sind " + k.toString());
    }

    @Override
    public FieldType getFieldType() {
        return this.fieldType;
    }


}
