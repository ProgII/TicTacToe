package enums;

public enum FieldType {
    X("x"), O("o"), empty(" ");

    private String type;

    FieldType(String type){
        this.type = type;
    }
    public String toString() {
       return type;
    }

}
