package main;

import java.io.IOException;

import game.Commentator;
import game.Game;
import observer.GameObserver;
import player.KeyBoardPlayer;
import player.Player;

public class Main {

    public static void main(String args[]) throws IOException {

        Game game = new Game();

        //TODO: initialise Player2
        Player player1 = new KeyBoardPlayer();
        Player player2 = null;
        game.joinPlayer1(player1); // Spieler treten dem Spiel bei
        game.joinPlayer2(player2);

        GameObserver commentator = new Commentator(); // erstellt einen
                                                      // Kommentator,
                                                      // der das Spiel über
                                                      // die Kommandozeile
                                                      // kommentiert


        game.addObserver(commentator);
        game.run();
    }
}
