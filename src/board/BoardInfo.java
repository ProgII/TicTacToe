package board;

import enums.FieldType;

public interface BoardInfo {
	
    /**
     * Gibt den Stein an der Stelle (x,y) zurück.
     *
     * @param x
     *            Zeile
     * @param y
     *            Spalte
     * @return der Stein an dieser Stelle
     */
     FieldType getFieldInfo(int x, int y);
     
     /**
      * Testet ob das Board voll ist.
      * 
      * @return true genau dann, wenn das Board voll ist.
      */
      boolean isFull();
      
      /**
       * 
       * @param x
       *            Zeile
       * @param y
       *            Spalte
       * @return true , wenn das Feld frei ist
       */
       public boolean canSetPiece(int  x, int y);

  	

}
