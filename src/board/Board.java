package board;

import enums.FieldType;

public class Board {

    private final FieldType board[][] = new FieldType[3][3];

    public Board() {
       //TODO: implement this
    }
    
    /**
     * 
     * @return BoardInfo zu diesem Board
     */
    public BoardInfo getBoardInfo(){
    	return null;
    	//TODO: implement this	
    }
	
    /**
     * Setzt einen Stein an die Stelle (x, y).
     * 
     * @param fieldType
     *            das Zeichen des Spielers
     * @param x
     *            Spalte
     * @param y
     *            Zeile
     */
     public void setPiece(FieldType fieldType, int x, int y) {
    	//TODO: implement this
     }

    /**
     * Gibt den Stein an der Stelle (x,y) zurück.
     *
     * @param x
     *            Spalte
     * @param y
     *            Zeile
     * @return der Stein an dieser Stelle
     */
     public FieldType getFieldInfo(int x, int y) {
    	//TODO: implement this
    	 return null;
	}

    /**
     * 
     * @param x
     *            Zeile
     * @param y
     *            Spalte
     * @return true , wenn das Feld frei ist
     */
     public boolean canSetPiece(int  x, int y) {
    	//TODO: implement this
    	 return false;
	}

    /**
     * Testet ob das Board voll ist.
     * 
     * @return true genau dann, wenn das Board voll ist.
     */
     public boolean isFull() {
    	//TODO: implement this
         return false;
	}


}
