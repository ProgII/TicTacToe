package game;

import enums.FieldType;
import observer.GameObserver;

/**
 * Diese Klasse ist nur ein GameObserver, der das Spiel kommentiert, indem die
 * erhaltenen Events in der Kommandozeile ausgegeben werden.
 */
public class Commentator implements GameObserver {


    public Commentator() {
    }

    @Override
    public void pieceWasSet(Turn t) {
        System.out.println("Kommentator: Spieler " + t.fieldType + " war am Zug und hat (" + t.x + "," + t.y + ") besetzt.");

    }

    @Override
    public void gameHasEnded(FieldType type) {
        if (type != FieldType.empty) {
            System.out.println("Kommentator: Der Gewinner ist " + type + "!");
        } else {
            System.out.println("Kommentator: Unentschieden!");
        }
    }


}
