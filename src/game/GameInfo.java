package game;

import board.BoardInfo;
import enums.FieldType;
import observer.GameObserver;

public interface GameInfo {
	
	/**
	 * @return Gibt das Board zurück.
	 */
	public BoardInfo getBoardInfo();
	
	/**
	 * Diese Methode testet, ob es einen Sieger gibt.
	 * 
	 * @return true genau dann, wenn es einen Sieger gibt
	 */
	public boolean hasWinner();
	
	/**
	 * Gibt den Gewinner zurück.
	 * 
	 * @return der Gewinner
	 */
	public FieldType getWinner();

  	/**
  	 * Diese Methode überprüft, ob das Spiel vorbei ist.
  	 * 
  	 * @return true genau dann, wenn das Spiel vorbei ist
  	 */
  	public boolean isOver();
  	
	/**
	 * Fügt einen Observer hinzu, der benachrichtigt werden muss
	 */

	public void addObserver(GameObserver gameObserver);

	/**
	 * Entfernt einen Observer
	 */

	public void removeObserver(GameObserver gameObserver);
	
}
