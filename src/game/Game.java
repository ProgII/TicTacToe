package game;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

import board.Board;
import board.BoardInfo;
import enums.FieldType;
import observer.GameObservable;
import observer.GameObserver;
import player.Player;

public class Game implements GameObservable {
	private FieldType winner = FieldType.empty;
	private Board board;
	private ArrayList<GameObserver> gameObserverList;

	private Player player1 = null;
	private Player player2 = null;

	public Game() {
		this.board = new Board();
		this.gameObserverList = new ArrayList<GameObserver>();
	}

	/**
	 * @return Gibt das Board zurück.
	 */
	public Board getBoard() {
		return this.board;
	}

	/**
	 * Startet und steuert das Spiel
	 */
	public void run() throws IOException {
		
		player1.setFieldType(FieldType.X); // Spieler erhalten ihr FieldType
		player2.setFieldType(FieldType.O);

		Random r = new Random();

		//TODO: implement this
		
		// zufälligen Startspieler Wählen

		// Board anzeigen

		// Spielablauf: Solange das Spiel nicht vorbei ist:

			//Wähle aktuellen Spieler aus
		
			//Wähle gültigen Zug und führe ihn aus
		
			//Observer informieren
		
			//Board anzeigen
		}

	/**
	 * Diese Methode testet, ob es einen Sieger gibt.
	 * 
	 * @return true genau dann, wenn es einen Sieger gibt
	 */
	public boolean hasWinner() {
		// TODO: implement this
		return false;
	}

	/**
	 * Diese Methode überprüft, ob das Spiel vorbei ist.
	 * 
	 * @return true genau dann, wenn das Spiel vorbei ist
	 */
	public boolean isOver() {
		// TODO: implement this
		return false;
	}

	/**
	 * Diese Methode printet das Spiel auf die Kommandozeile.
	 */
	public void showBoard() {
		for (int i = 0; i < 3; i++) {
			String row = "|";
			for (int j = 0; j < 3; j++) {
				row = row + this.board.getFieldInfo(j, i) + "|";
			}
			System.out.println(row);
		}
	}

	/**
	 * Lässt den 1. Spieler beitreten.
	 * 
	 * @param player
	 *            Spieler 1
	 */
	public void joinPlayer1(Player player) {
		this.player1 = player;
	}

	/**
	 * Lässt den 2. Spieler beitreten.
	 * 
	 * @param player
	 *            Spieler 2
	 */
	public void joinPlayer2(Player player) {
		this.player2 = player;
	}

	/**
	 * Gibt den Gewinner zurück.
	 * 
	 * @return der Gewinner
	 */
	public FieldType getWinner() {
		// TODO: implement this
		return null;
	}

	/**
	 * lässt sich vom Player einen Turn geben wenn dieser nicht gesetzt werden
	 * kann, fordert es einen neuen Zug an
	 * 
	 * @param p
	 * @return
	 */
	private Turn getValidTurn(Player p) {
		return null;
		// TODO: implement this
	}

	/**
	 * @return Eine GameInfo die zum aktuellen Game gehört
	 */
	public GameInfo getGameInfo() {
		return null;
		// TODO: implement this
	}

	/**
	 * @return Die zugehörige Information zum Board
	 */
	public BoardInfo getBoardInfo() {
		// TODO: implement this
		return null;
	}

	/**
	 * Fügt einen Observer hinzu, der benachrichtigt werden muss
	 */

	@Override
	public void addObserver(GameObserver gameObserver) {
		// TODO: implement this
	}

	/**
	 * Entfernt einen Observer
	 */

	@Override
	public void removeObserver(GameObserver gameObserver) {
		// TODO: implement this
	}

	/**
	 * Informiert die Observer über den Zug
	 * 
	 * @param t
	 */
	public void notifyObserversPieceWasSet(Turn t) {
		// TODO: implement this
	}

	/**
	 * Informiert die Observer über das Ende des Spiels
	 * 
	 * @param type
	 */

	public void notifyObserversGameHasEnded(FieldType type) {
		// TODO: implement this
	}

}
