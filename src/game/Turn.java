package game;

import enums.FieldType;

/**
 * Diese Klasse repräsentiert einen Spielzug.
 */
public class Turn {

    public final int x;
    public final int y;
    public final FieldType fieldType;

    public Turn(int x, int y, FieldType fieldType) {
        this.x = x;
        this.y = y;
        this.fieldType = fieldType;
    }

}
