package game;

import board.BoardInfo;
import enums.FieldType;
import observer.GameObserver;

public class GameInfoImpl implements GameInfo{
	private Game game;
	
	public GameInfoImpl(Game game){
		this.game = game;
	}
	@Override
	public BoardInfo getBoardInfo() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean hasWinner() {
		return game.hasWinner();
	}

	@Override
	public FieldType getWinner() {
		return game.getWinner();
	}

	@Override
	public boolean isOver() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void addObserver(GameObserver gameObserver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void removeObserver(GameObserver gameObserver) {
		// TODO Auto-generated method stub
		
	}

}
